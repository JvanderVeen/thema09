---
title: "log"
author: "Jurjen van der Veen"
date: "9/10/2019"
output:
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(ggplot2)
library(tidyr)
library(ggcorrplot)
```

## Introduction
  
Birds can be categorised into different groups depending on their environment. These groups would include:
group              |abbreviation
-------------------|---------------------
Raptors            |R
Swimming birds     |SW
wadding birds      |W
terrestrial birds  |T
scansorial birds   |P
Singing birds      |SO


Data scientist examined the difference of bone size/shape within the ecological groups (the dataset is public, though there was no article published). This was done since the diffent groups rely on different parts of their bodies. This data could be used to predict what group a bird belongs into based on the size and diameter of its bones. This is usefull for identifying birds.
However, which of the different measured bones is the best to look at to find the correct ecological group? This research will answer the question.
The dataset used in this research includes 10 attributes.
The attributes are described in the table below

column name       |Attribute                                      |Data type    |Range
------------------|-----------------------------------------------|-------------|------------
huml              |Length of Humerus (mm)                         |float        |9.85-420.0
humw              |Diameter of Humerus (mm)                       |float        |1.14-17.8
ulnal             |Length of Ulna (mm)                            |float        |14.1-422
ulnaw             |Diameter of Ulna (mm)                          |float        |1.0-12.0
feml              |Length of Femur (mm)                           |float        |11.8-117.0
femw              |Diameter of Femur (mm)                         |float        |0.93-11.6
tibl              |Length of Tibiotarsus (mm)                     |float        |5.5-240.0
tibw              |Diameter of Tibiotarsus (mm)                   |float        |0.87-11.0
tarl              |Length of Tarsometatarsus (mm)                 |float        |7.77-175.0
tarw              |Diameter of Tarsometatarsus (mm)               |float        |0.66-14.1
Type              |Ecological Group                               |factor       |NA







## Data exploring
Before answering the research question it is important to look at the data and remove any instances with missing values, since missing values do not tell anything in this dataset.
```{r Data preparing, echo=FALSE}
# it's stated that missing values are represented by empty strings
my.data <- read.csv('bird.csv', na.strings = '')

```

First visualise a scatterplot of the different lengths of each bone as a function of the different diameter of each bone (figure 1). The data seems to be correlated. In figure 2 a boxplot is shown for the length of the bones. While figure 3 shows a boxplot for the diameter of the bones. These two boxplots confirm that the data is similar across all different bones. However, it also contains a lot of outliers. The data will be log2-transformed in order to combat this. As seen in figures 4 and 5 this takes care of most outliers.
```{r visualization, echo=FALSE}
#figure 1
bones <- gather(my.data[,2:11], key='bonetype',value = 'measure')
ggplot(data = my.data[,2:11]) +
  geom_point(aes(x=huml,y=humw, colour='Humerus'),na.rm = T) +
  geom_point(aes(x=ulnal,y=ulnaw,colour='Ulna'),na.rm = T) +
  geom_point(aes(x=feml,y=femw,colour='Femur'),na.rm = T) +
  geom_point(aes(x=tibl,y=tibw,colour='Tibiotarsus'),na.rm = T) +
  geom_point(aes(x=tarl,y=tarw,colour='tarsometatarsus'),na.rm = T) +
  labs(title = 'Dimensions of the different bones',
          caption = 'Figure 1')+
  xlab('Length (mm)') +
  ylab('Diameter (mm)')
# figure 2
ggplot(data=bones[c(1:420, 841:1260, 1681:2100,
                    2521:2940, 3361:3780),], aes(x=bonetype, y = measure)) +
  geom_boxplot(na.rm = T) +
  xlab('Bones') +
  ylab('Length (mm)') +
  labs(title = 'Length of the different bones',
          caption =  'Figure 2')
# figure 3
ggplot(data = bones[c(421:840, 1261:1680, 2101:2520,
                      2941:3360, 3781:4200),], aes(x=bonetype, y = measure))+
         geom_boxplot(na.rm = T) +
         xlab('Bones') +
         ylab('Diameter (mm)') +
  labs(title = 'Diameter of the different bones',
          caption = 'Figure 3')

# logged data
logged.data <- log2(my.data[2:11])
tidy.log <- gather(logged.data, key = "bone", value = "measure")

ggplot(data=tidy.log[c(1:420, 841:1260, 1681:2100,
                    2521:2940, 3361:3780),], aes(x=bone, y = measure)) +
  geom_boxplot(na.rm = T) +
  xlab('Bones') +
  ylab('Logged Length (mm)') +
  labs(title = 'Logged length of the different bones',
          caption =  'Figure 4')

# figure 5
ggplot(data = tidy.log[c(421:840, 1261:1680, 2101:2520,
                      2941:3360, 3781:4200),], aes(x=bone, y = measure))+
         geom_boxplot(na.rm = T) +
         xlab('Bones') +
         ylab('Logged diameter (mm)') +
  labs(title = 'Logged diameter of the different bones',
          caption = 'Figure 5')
```

A histogram is showing the frequency each bird group is present in the dataset (figure 6). It shows how the groups are not equally present in the dataset. 
```{r class split,echo=FALSE}

ggplot(data = my.data,aes(x = type)) +
     geom_histogram(na.rm = T, stat = "count") + 
  labs(title = "Frequency of all ecological groups",
       caption = "Figure 6") +
  xlab("group")
```

## Data cleaning
  
All missing values do not tell anything about this dataset, so it is best to delete all instances with missing values.
Since the length of the bone is correlated to the diameter of the bone, these attributes are used to make a volume attribute for each bone instead. All bones are cylinders, so the formula $$ V=B*h $$ was used with B being the diameter and h representing length. First the regular dataset was used to calculate the volume (figure 7). Afterwards the log2-transformed data was used, since it was less sensitive to outliers(figure 8). In this figure the data was more correlated then when using the not log-transformed data.
```{r data cleaning, echo=FALSE}
# remove NAs
# +1 since ids start at 0
NA.ids <- my.data[apply(my.data,1, anyNA),1]
removed.data <- my.data[- (NA.ids+1),]

# unlogged volume
volume.hum <- removed.data$huml * removed.data$humw
volume.ulna <- removed.data$ulnal * removed.data$ulnaw
volume.fem <- removed.data$feml * removed.data$femw
volume.tib <- removed.data$tibl * removed.data$tibw
volume.tar <- removed.data$tarl * removed.data$tarw

new.data <- data.frame(volume.hum, volume.ulna,
                       volume.fem, volume.tib,
                       volume.tar, removed.data$type)
# figure 7
correlation <- cor(new.data[1:5])
ggcorrplot(correlation, lab = T)+
  labs(caption = "figure 7",
       title = "Correlation volume (not log-transformed)")
```


```{r log volume, echo=FALSE}
# logged volume
logged.volume.hum <- log2(volume.hum)
logged.volume.ulna <- log2(volume.ulna)
logged.volume.fem <- log2(volume.fem)
logged.volume.tib <- log2(volume.tib)
logged.volume.tar <- log2(volume.tar)

new.log.data <- data.frame(logged.volume.hum, logged.volume.ulna,
                           logged.volume.fem, logged.volume.tib,
                           logged.volume.tar, removed.data$type)
# figure 8
log.corr <- cor(new.log.data[1:5])
ggcorrplot(log.corr, lab = T)+
  labs(caption = "figure 8",
       title = "Correlation volume (log-transformed)")

clean.data <- as.data.frame(scale(new.log.data[1:5]))
clean.data[6] <- new.log.data[6]
colnames(clean.data) <- c("volume.hum", "volume.ulna", "volume.fem",
                          "volume.tib","volume.tar", "type")

# export cleaned data
write.csv(clean.data, file = "cleaned_bird.csv",
          row.names = F)

```

## Data models
  
The cleaned data is used in weka to test varius algorithms in order to gain a model. The cost-sensitive classifier favors false-negatives over false-positives. A false-positive in one group classifies as a false-negative in another group. In order to still classify false-positives from false-negatives, the largest group (singing birds) will function as the positive group. For all algorithms the default values are selected and are 10-fold cross validated. After using the experimenter application the models are evaluated against each other. This is done by looking at the area under the ROC, since it gives a score that takes both false-positives as false-negatives into account. The table below describes all algorithms used.

algorithm                   |description      
----------------------------|-------------------------------
zeroR                       | Classifies all instances to the class that appears the most.
oneR                        | Classifies all instances using the best attribute.
naive bayes                 | Uses the bayes formula ($P(c|a1,...,a5) = \frac{(P(a1|c) * P(a2|c) * ... * P(c)}{P(a1,a2,...)}$) to classifie all instances.
logistic                    | Uses logistic regression (P = F(a1x1+a1x2+...+C)) to classifie instances.
Support vector machine(SVM) | Tries to draw a straight line through the data who's trajectory is impacted by the data points.
nearest neighbor            | Classifies the data points as the same class as it's closest neighbor
J48                         | Makes a tree of all instances using multiple attributes to classifie the instances.



# Comparing models
```{r model loading,echo=FALSE}
algorithms <- read.csv("algorithms.csv", na.strings = "NaN")

create.confusion.matrix <- function(algorithms,field){
  
  confusion.matrix <- data.frame()
  # Iterate per algorithm this is done by seperating the dataframe on an unique field
  for (algorithm in split(algorithms, field)) {
    calc <- data.frame()
    
    for (run in split(algorithm, algorithm$Key_Run)) {
      # Add the values for the true positives (TP), true negatives (TN),
      # false positives (FP), false negatives (FN), accuracy (acc),
      # ROC, time to a dataframe to append to the calc dataframe
      run.calc <- data.frame(
      med.TP <- median(run$True_positive_rate),
      med.TN <- median(run$True_negative_rate),
      med.FP <- median(run$False_positive_rate),
      med.FN <- median(run$False_negative_rate),
      med.acc <- median(run$Percent_correct),
      med.roc <- median(run$Area_under_ROC),
      med.time <- median(run$Elapsed_Time_training) + median(run$Elapsed_Time_testing))
      calc <- rbind(calc,run.calc)
    }
    # Combine the values into 1 value by taking the mean and add it to the
    # confusion matrix
    final <- data.frame(matrix(apply(calc, MARGIN = 2, FUN = mean)
                               , ncol=7),row.names = algorithm$Key_Scheme[1])
      names(final)[names(final)=="X1"] <- "TP"
    names(final)[names(final)=="X2"] <- "TN"
    names(final)[names(final)=="X3"] <- "FP"
    names(final)[names(final)=="X4"] <- "FN"
    names(final)[names(final)=="X5"] <- "Accuracy"
    names(final)[names(final)=="X6"] <- "ROC"
    names(final)[names(final)=="X7"] <- "Time"
    colnames(final)
    confusion.matrix <- rbind(confusion.matrix, final)
  }
  return(confusion.matrix)
  }
models.confusion <- create.confusion.matrix(algorithms,
                                            algorithms$Key_Scheme)
```

## Meta learners

For the meta learners the following algorithms have been used: voting (using IBK (k=20), logistics, SMO, naive bayes, J48), stacking (IBK (k=20), logistics,SMO, naive bayes to random forest), bagging (for J48, logistics, naive bayes), boosting (J48,logistics,naive bayes). The data is saved in the meta.csv file and is compared.

```{r meta, echo=FALSE}
meta.learners <- read.csv("meta.csv", na.strings = "NaN")
meta.confusion <- create.confusion.matrix(meta.learners, 
                                          meta.learners$Key_Scheme_options)


```

## Java

The final model has been used in a java application. The default data is created by splitting the cleaned data. While this would be a problem if the data was used to test the model, however since we simply classify the instances as an example, there is nothing wrong with it.
```{r split data}
part1 <- clean.data[1:413%%2 == 0,]
part2 <- clean.data[1:413%%2 == 1,]
write.csv(part1, file = "training_data.csv", row.names = F)
part2 = part2[,1:5]
write.csv(part2, file = "test_data.csv", row.names = F)
```

https://www.kaggle.com/zhangjuefei/birds-bones-and-living-habits
